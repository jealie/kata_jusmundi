# A Kata

The following Python package implements and tests two functions:

- `number_to_french_word`, which converts a single number to its French word

- `numbers_to_french_words` (mind the two `s`) which extends the above to a list

Code demo:

```
make build
make run
```

and inside the docker:

```
pytest tests/numbers.py
```
