FROM python:3.10

WORKDIR /workspace

COPY requirements.python ./
RUN pip3 install --no-cache-dir -r requirements.python
