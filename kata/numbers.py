def number_to_french_word(number: int, ends: bool=True):
    french_numbers = {
        0: 'zéro', 1: 'un', 2: 'deux', 3: 'trois', 4: 'quatre', 5: 'cinq',
        6: 'six', 7: 'sept', 8: 'huit', 9: 'neuf', 10: 'dix',
        11: 'onze', 12: 'douze', 13: 'treize', 14: 'quatorze', 15: 'quinze',
        16: 'seize', 20: 'vingt', 30: 'trente', 40: 'quarante', 50: 'cinquante',
        60: 'soixante', 70: 'soixante-dix', 71: 'soixante-et-onze', 72: 'soixante-douze',
        73: 'soixante-treize', 74: 'soixante-quatorze', 75: 'soixante-quinze',
        76: 'soixante-seize', 80: 'quatre-vingt', 81: 'quatre-vingt-un',
        82: 'quatre-vingt-deux', 83: 'quatre-vingt-trois', 84: 'quatre-vingt-quatre',
        85: 'quatre-vingt-cinq', 86: 'quatre-vingt-six', 87: 'quatre-vingt-sept',
        88: 'quatre-vingt-huit', 89: 'quatre-vingt-neuf', 90: 'quatre-vingt-dix',
        91: 'quatre-vingt-onze', 92: 'quatre-vingt-douze', 93: 'quatre-vingt-treize',
        94: 'quatre-vingt-quatorze', 95: 'quatre-vingt-quinze', 96: 'quatre-vingt-seize',
        100: 'cent', 1000: 'mille', 1000000: 'million'
    }

    assert (number == int(number))

    if number in french_numbers:
        return french_numbers[number]

    if number < 17:
        return french_numbers[number]
    elif number < 20:
        return french_numbers[10] + '-' + french_numbers[number - 10]
    elif number == 80:
        if ends:
            return french_numbers[80] + 's'
        else:
            return french_numbers[80]
    elif number < 100:
        tens = number // 10
        remainder = number % 10
        if remainder == 0:
            return french_numbers[tens * 10]
        elif remainder == 1:
            return french_numbers[tens * 10] + '-et-' + french_numbers[1]
        else:
            return french_numbers[tens * 10] + '-' + french_numbers[remainder]
    elif number < 1000:
        hundreds = number // 100
        remainder = number % 100
        if hundreds == 1:
            start = french_numbers[100]
        else:
            start = french_numbers[hundreds] + '-' + french_numbers[100]
        if remainder == 0:
            if hundreds > 1 and ends:
                return start + 's'
            else:
                return start
        else:
            return start + '-' + number_to_french_word(remainder, ends=ends)
    elif number < 1000000:
        thousands = number // 1000
        remainder = number % 1000
        if thousands == 1:
            start = french_numbers[1000]
        else:
            start = number_to_french_word(thousands, ends=False) + '-' + french_numbers[1000]
        if remainder == 0:
            if ends:
                return start + 's'
            else:
                return start
        else:
            return start + '-' + number_to_french_word(remainder, ends=ends)
    else:
        millions = number // 1000000
        remainder = number % 1000000
        if remainder == 0:
            return number_to_french_word(millions) + '-' + french_numbers[1000000]
        else:
            return number_to_french_word(millions) + '-' + french_numbers[1000000] + ' ' + number_to_french_word(remainder)

def numbers_to_french_words(numbers_list):
    return [number_to_french_word(num) for num in numbers_list]

