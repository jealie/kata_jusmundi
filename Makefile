help:
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the docker
	docker build --no-cache -t kata_jusmundi .

run: ## Run the docker
	docker run  -v `pwd`:/workspace/ -it kata_jusmundi /bin/bash

enter-docker: ## enter the earliest running docker
	docker exec -u root -it `sudo docker ps -a | grep kata_jusmundi | tail -n 1 | cut -f1 -d' '` /bin/bash
